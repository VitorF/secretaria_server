package br.com.secretaria.usuario;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class UsuarioFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	
	
	
	public Usuario salvar(Usuario usuario) {
		manager.persist(usuario);

		return usuario;
	}

	
	
	
	
	public List<Usuario> findAll() {
		TypedQuery<Usuario> query = manager.createQuery("SELECT f FROM " + Usuario.NAME + " f", Usuario.class);

		return query.getResultList();
	}

	
	
	
	
	public void remover(Integer usuarioId) {
		manager.remove(manager.find(Usuario.class, usuarioId));
	}

	
	
	
	public Usuario findPrimaryKey(Integer usuarioId) {
		return manager.find(Usuario.class, usuarioId);
	}

	
	
	
	
	public Usuario FindToFuncionario(Long funcionarioId) {
		try {
			TypedQuery<Usuario> query = manager.createQuery(
					" SELECT u FROM " + Usuario.NAME + " u WHERE u.funcionario.funcionarioId = :funcionarioId",
					Usuario.class);

			query.setParameter("funcionarioId", funcionarioId);
			return query.getSingleResult();

		} catch (NoResultException e) {
			return null;
		}

	}
	
	
	public Usuario login(String login) {
		try {
			TypedQuery<Usuario> query = manager
					.createQuery(" SELECT u FROM " + Usuario.NAME + " u WHERE u.login = :login", Usuario.class);

			query.setParameter("login", login);
			return query.getSingleResult();

		} catch (NoResultException e) {
			return null;
		}

	}

}
