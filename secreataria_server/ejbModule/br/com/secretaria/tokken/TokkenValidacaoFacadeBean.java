package br.com.secretaria.tokken;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class TokkenValidacaoFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public TokkenValidacao salvar(TokkenValidacao titularidade) {
		manager.persist(titularidade);
		return titularidade;
	}
	
	
	
	public TokkenValidacao findTokkenCodigo(String codigo){
		try{
			TypedQuery<TokkenValidacao> query  =  manager.createQuery("SELECT t FROM "+TokkenValidacao.NAME+" t WHERE t.codigo = :codigo and t.validado = false",TokkenValidacao.class);
		
		
		query.setParameter("codigo",codigo);
		
		return query.getSingleResult();
		
		}catch(NoResultException e) {
			return null;
		}
	}
	
	
	public TokkenValidacao alterar(TokkenValidacao tokkenValidacao) {
		return manager.merge(tokkenValidacao);
	}
	
	
	
	
}
