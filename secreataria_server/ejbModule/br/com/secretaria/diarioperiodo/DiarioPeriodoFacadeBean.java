package br.com.secretaria.diarioperiodo;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class DiarioPeriodoFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public DiarioPeriodo salvar(DiarioPeriodo diarioPeriodo) {
		manager.persist(diarioPeriodo);
		return diarioPeriodo;
	}

	public DiarioPeriodo alterar(DiarioPeriodo diarioPeriodo) {
		return manager.merge(diarioPeriodo);
	}

	public void remover(Integer diarioPeriodoId) {
		manager.remove(manager.find(DiarioPeriodo.class, diarioPeriodoId));
	}

	public List<DiarioPeriodo> findAll() {

		return null;
	}

	public List<DiarioPeriodo> findPorModeloPeriodoDiario(Integer modeloPeriodoDiarioId) {
		TypedQuery<DiarioPeriodo> query = manager.createQuery(
				"SELECT dp FROM " + DiarioPeriodo.NAME
						+ " dp WHERE  dp.modeloPeriodoDiario.modeloPeriodoDiarioId = :modeloPeriodoDiarioId",
				DiarioPeriodo.class);
		query.setParameter("modeloPeriodoDiarioId", modeloPeriodoDiarioId);
		return query.getResultList();
	}

}
