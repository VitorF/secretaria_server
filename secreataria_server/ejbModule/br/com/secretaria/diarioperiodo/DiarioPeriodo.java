package br.com.secretaria.diarioperiodo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.ma.secretaria.periodo.Periodo;
import br.com.secretaria.diario.Diario;
import br.com.secretaria.itemdiarioperiodo.ItemDiarioPeriodo;
import br.com.secretaria.modeloperiododiario.ModeloPeriodoDiario;

@Entity(name = DiarioPeriodo.NAME)
@Table(name = "educ_diario_periodo", schema = "educacao")
public class DiarioPeriodo implements Serializable {
	public static final String NAME = "educacao_diario_periodo";

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "diario_periodo_id")
	private Integer diarioPeriodoId;

	@ManyToOne
	@JoinColumn(name = "periodo_id")
	private Periodo periodo;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio")
	private Date dataInicio;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_fim")
	private Date dataFim;
	
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_entrega")
	private Date dataEntrega;
   
	@Column(name = "observacao")
	private String observacao;

	@ManyToOne
	@JoinColumn(name = "modelo_periodo_diario_id")
	private ModeloPeriodoDiario modeloPeriodoDiario;
	
	
	@ManyToOne
	@JoinColumn(name = "diario_id")
	private Diario diario;
	
	@OneToMany(mappedBy = "diarioPeriodo",cascade = CascadeType.REMOVE)
	private List<ItemDiarioPeriodo> itens;
	
	
	
	
	
	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public Diario getDiario() {
		return diario;
	}
	
	public void setDiario(Diario diario) {
		this.diario = diario;
	}

	public Integer getDiarioPeriodoId() {
		return diarioPeriodoId;
	}

	public void setDiarioPeriodoId(Integer diarioPeriodoId) {
		this.diarioPeriodoId = diarioPeriodoId;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public ModeloPeriodoDiario getModeloPeriodoDiario() {
		return modeloPeriodoDiario;
	}

	public void setModeloPeriodoDiario(ModeloPeriodoDiario modeloPeriodoDiario) {
		this.modeloPeriodoDiario = modeloPeriodoDiario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((diarioPeriodoId == null) ? 0 : diarioPeriodoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DiarioPeriodo other = (DiarioPeriodo) obj;
		if (diarioPeriodoId == null) {
			if (other.diarioPeriodoId != null)
				return false;
		} else if (!diarioPeriodoId.equals(other.diarioPeriodoId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DiarioPeriodo [diarioPeriodoId=" + diarioPeriodoId + ", periodo=" + periodo + ", dataInicio="
				+ dataInicio + ", dataFim=" + dataFim + ", observacao=" + observacao + ", modeloPeriodoDiario="
				+ modeloPeriodoDiario + "]";
	}
	
	
	

}
