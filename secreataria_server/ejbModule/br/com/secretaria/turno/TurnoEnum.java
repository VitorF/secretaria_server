package br.com.secretaria.turno;

public enum TurnoEnum {
	
	VESPERTINO("Vespertino") , MATUTINO("matutino") , NOTURNO("noturno");
	
	private String descricao;
	TurnoEnum(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
