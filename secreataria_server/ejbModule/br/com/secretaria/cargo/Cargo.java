package br.com.secretaria.cargo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.ma.secretaria.tipocargoEnum.TipoCargoEnum;
import br.com.secretaria.tipofuncaoenum.TipoFuncaoEnum;

@Entity(name = Cargo.NAME)
@Table(name = "educacao.educ_cargo")
public class Cargo implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String NAME = "educ_cargo";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "cargo_id")
	private Integer cargoId;
	
	@NotEmpty(message="campo 'Descri��o' obrigatorio !!")
	@Column(name = "descricao")
	private String descricao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_cargo")
	private TipoCargoEnum tipoCargoEnum;
//	
//	@Enumerated(EnumType.STRING)
//	@Column(name="tipo_funcao_enum")
//	private TipoFuncaoEnum tipoFuncaoEnum;
//	
	
	
	
	
//	public TipoFuncaoEnum getTipoFuncaoEnum() {
//		return tipoFuncaoEnum;
//	}
//	public void setTipoFuncaoEnum(TipoFuncaoEnum tipoFuncaoEnum) {
//		this.tipoFuncaoEnum = tipoFuncaoEnum;
//	}
	public Integer getCargoId() {
		return cargoId;
	}
	public void setCargoId(Integer cargoId) {
		this.cargoId = cargoId;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public TipoCargoEnum getTipoCargoEnum() {
		return tipoCargoEnum;
	}
	public void setTipoCargoEnum(TipoCargoEnum tipoCargoEnum) {
		this.tipoCargoEnum = tipoCargoEnum;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cargoId == null) ? 0 : cargoId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cargo other = (Cargo) obj;
		if (cargoId == null) {
			if (other.cargoId != null)
				return false;
		} else if (!cargoId.equals(other.cargoId))
			return false;
		return true;
	}
	
			
			
			
	@Override
	public String toString() {
		return "Cargo [cargoId=" + cargoId + ", descricao=" + descricao + ", tipoCargoEnum=" + tipoCargoEnum + "]";
	}
	
	
	
	
	

}
