package br.com.secretaria.cargo;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class CargoFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public Cargo salvar(Cargo cargo) {
		manager.persist(cargo);
		return cargo;
	}

	public Cargo alterar(Cargo cargo) {
		return manager.merge(cargo);
	}

	public void excluir(Integer cargoId) {
		manager.remove(manager.find(Cargo.class, cargoId));
	}
	
	public Cargo findByPrimaryKey(Integer cargoId) {
		return manager.find(Cargo.class, cargoId);
	}
	
	public List<Cargo> findAll(){
		TypedQuery<Cargo> query = manager.createQuery("SELECT c FROM "+Cargo.NAME+" c order by c.tipoCargoEnum",Cargo.class);
	 return query.getResultList();
	}
}
