package br.com.secretaria.funcionariotitularidae;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.ma.secretaria.funcionario.Funcionario;
import br.com.secretaria.titularidade.Titularidade;

@Entity(name=FuncionarioTitularidade.NAME)
@Table(name="educacao.educ_funcionario_titularidade")
public class FuncionarioTitularidade implements Serializable {


	private static final long serialVersionUID = 1L;
	public static final String NAME = "educ_funcionario_titularidade";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="func_titularidade_id")
	private Long  funcionarioTitularidadeId;
	@ManyToOne
	@JoinColumn(name="fucionario_id")
	private Funcionario funcionario;
	@ManyToOne
	@JoinColumn(name="titularidade")
	private Titularidade titularidade;
	
	
	public Long getFuncionarioTitularidadeId() {
		return funcionarioTitularidadeId;
	}
	public void setFuncionarioTitularidadeId(Long funcionarioTitularidadeId) {
		this.funcionarioTitularidadeId = funcionarioTitularidadeId;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public Titularidade getTitularidade() {
		return titularidade;
	}
	public void setTitularidade(Titularidade titularidade) {
		this.titularidade = titularidade;
	}
	
	
	
	
}
