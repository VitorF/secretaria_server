package br.com.secretaria.funcionariotitularidae;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.ma.secretaria.funcionario.Funcionario;
import br.com.secretaria.titularidade.Titularidade;

@Stateless
public class FuncionarioTitularidadeFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public FuncionarioTitularidade salvar(FuncionarioTitularidade funcionarioTitularidade) {
		manager.persist(funcionarioTitularidade);
		return funcionarioTitularidade;
	}

	public List<FuncionarioTitularidade> buscaTitularidadesPorFuncionario(Funcionario funcionario) {
		TypedQuery<FuncionarioTitularidade> query = manager.createQuery(
				"SELECT ft FROM " + FuncionarioTitularidade.NAME + " ft WHERE ft.funcionario = :funcionario",
				FuncionarioTitularidade.class);
		query.setParameter("funcionario", funcionario);
		return query.getResultList();
	}

	public void excluir(FuncionarioTitularidade funcionarioTitularidade) {
		manager.remove(manager.find(FuncionarioTitularidade.class, funcionarioTitularidade.getFuncionarioTitularidadeId()));
	}
}
