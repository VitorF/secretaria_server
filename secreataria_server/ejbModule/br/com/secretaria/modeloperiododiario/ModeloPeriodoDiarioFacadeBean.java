package br.com.secretaria.modeloperiododiario;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class ModeloPeriodoDiarioFacadeBean {
	
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public ModeloPeriodoDiario salvar(ModeloPeriodoDiario modeloPeriodoDiario) {
		manager.persist(modeloPeriodoDiario);
		return modeloPeriodoDiario;
	}
	
	
	

	public ModeloPeriodoDiario alterar(ModeloPeriodoDiario modeloPeriodoDiario) {
		return manager.merge(modeloPeriodoDiario);
	}
	
	
	
	
	public void remover(Integer modeloPeriodoDiarioId) {
	    manager.remove(manager.find(ModeloPeriodoDiario.class, modeloPeriodoDiarioId));	
	}
	
	
	
	
	public List<ModeloPeriodoDiario> findAll(){

		
		TypedQuery<ModeloPeriodoDiario> query  =  manager.createQuery("SELECT m FROM  "+ModeloPeriodoDiario.NAME+" m ",ModeloPeriodoDiario.class);
	
		return query.getResultList();
	} 
	
	
	public List<ModeloPeriodoDiario> findToModeloDiario(Integer modeloDiarioId){

		
		TypedQuery<ModeloPeriodoDiario> query  =  manager.createQuery("SELECT m FROM  "+ModeloPeriodoDiario.NAME+" m where m.modeloDiario.modeloDiarioId = :modeloDiarioId ",ModeloPeriodoDiario.class);
	
		query.setParameter("modeloDiarioId", modeloDiarioId);
		return query.getResultList();
	} 

}
