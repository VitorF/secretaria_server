package br.com.secretaria.modeloperiododiario;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.ma.secretaria.periodo.Periodo;
import br.com.secretaria.modelodiario.ModeloDiario;
@Entity(name=ModeloPeriodoDiario.NAME)
@Table(name = "educ_modelo_periodo_diario",schema = "educacao")
public class ModeloPeriodoDiario implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NAME = "educacao_modelo_periodo_diario";
	
	
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	@Column(name="modelo_periodo_diario_id")
	private Integer modeloPeriodoDiarioId;
	
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio")
	private Date dataInicio;
	
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_fim")
	private Date dataFim;
	
	@ManyToOne
	@JoinColumn(name = "periodo_id")
	private Periodo periodo;
	
	@ManyToOne
	@JoinColumn(name = "modelo_diario_id")
	private ModeloDiario modeloDiario;
	
	
	
	
	

	public Integer getModeloPeriodoDiarioId() {
		return modeloPeriodoDiarioId;
	}

	public void setModeloPeriodoDiarioId(Integer modeloPeriodoDiarioId) {
		this.modeloPeriodoDiarioId = modeloPeriodoDiarioId;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public ModeloDiario getModeloDiario() {
		return modeloDiario;
	}

	public void setModeloDiario(ModeloDiario modeloDiario) {
		this.modeloDiario = modeloDiario;
	}


	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((modeloPeriodoDiarioId == null) ? 0 : modeloPeriodoDiarioId.hashCode());
		result = prime * result + ((periodo == null) ? 0 : periodo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModeloPeriodoDiario other = (ModeloPeriodoDiario) obj;
		if (modeloPeriodoDiarioId == null) {
			if (other.modeloPeriodoDiarioId != null)
				return false;
		} else if (!modeloPeriodoDiarioId.equals(other.modeloPeriodoDiarioId))
			return false;
		if (periodo == null) {
			if (other.periodo != null)
				return false;
		} else if (!periodo.equals(other.periodo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ModeloPeriodoDiario [modeloPeriodoDiarioId=" + modeloPeriodoDiarioId + ", dataInicio=" + dataInicio
				+ ", dataFim=" + dataFim + ", periodo=" + periodo + ", modeloDiario=" + modeloDiario + "]";
	}
	
	
	
	
	
	
	

}
