package br.com.secretaria.aluno;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

@Stateless
public class AlunoFacadeBean {
	
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;
	
	
	public Aluno salvar(Aluno aluno) {
		manager.persist(aluno);
		return aluno;
	}
	
	
	public Aluno update(Aluno aluno) {
		return manager.merge(aluno);
	}
	
	
	public void remove(Integer alunoId) {
		manager.remove(manager.find(Aluno.class, alunoId));
	}
	
	public Aluno findByPrimaryKey(Integer id) {
		return manager.find(Aluno.class, id);
	}
	
	
	public List<Aluno> findAll(){
	TypedQuery<Aluno> query  =  manager.createQuery("SELECT a FROM "+Aluno.NAME+" a ",Aluno.class);
	
	return query.getResultList();
	}
	
	
	public List<Aluno> findAlunosNaoMapeadosNoAno(Integer ano , Integer unidadeId){
		List<Aluno> alunos  =  new ArrayList<>();
		String sql  =  "select   " + 
				"result_aluno.aluno_id,  " + 
				"result_aluno.nome,  " + 
				"result_aluno.cpf  " + 
				"from   " + 
				"(  " + 
				"select   " + 
				"ea.aluno_id ,  " + 
				"ea.nome  ,   " + 
				"ea.cpf ,  " + 
				"(  " + 
				"select  " + 
				"count(*)   " + 
				"from  " + 
				"educacao.educ_turma_aluno eta  ,   " + 
				"educacao.educ_turma et   " + 
				"where   " + 
				"eta.turma_id  = et.turma_id   " + 
				"and et.ano = :ano  " + 
				"and et.unidade_unidade_id  = ea.unidadeCadastro  " + 
				"and eta.aluno_id  = ea.aluno_id   " + 
				") as tot_map  " + 
				"from   " + 
				"educacao.educ_aluno ea   " + 
				"where   " + 
				"ea.unidadeCadastro  = :unidadeId) result_aluno  " + 
				"where   " + 
				"result_aluno.tot_map  = 0";
		
		
		Query query = manager.createNativeQuery(sql);
		
		query.setParameter("unidadeId", unidadeId);
		query.setParameter("ano", ano);
		
		@SuppressWarnings("unchecked")
		List<Object[]> objects = (List<Object[]>)   query.getResultList();
		
		for (Object[] obj : objects) {
			alunos.add(new Aluno(Integer.parseInt(obj[0].toString()),(String)obj[1],(String) obj[2]));
		}
		
		return alunos;
	}

}
