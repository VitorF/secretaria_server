package br.com.secretaria.aluno;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.secretaria.unidade.Unidade;

@Entity(name = Aluno.NAME)
@Table(name = "educ_aluno")
public class Aluno implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String NAME = "secretaria_aluno";

	public Aluno() {

	}

	public Aluno(Integer alunoId, String nome, String cpf) {
		this.alunoId = alunoId;
		this.nome = nome;
		this.cpf = cpf;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "aluno_id")
	private Integer alunoId;

	@Column(name = "nome")
	private String nome;

	@Column(name = "cpf", unique = true)
	private String cpf;

	@Column(name = "rg", unique = true)
	private String rg;

	@Column(name = "email")
	private String email;

	@Column(name = "nome_mae")
	private String nomeMae;

	@Column(name = "nome_pai")
	private String nomePai;

	@Column(name = "endereco")
	private String endereco;

	@Column(name = "cidade")
	private String cidade;

	@Column(name = "data_nascimento")
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;

	@Column(name = "data_cadastro")
	@Temporal(TemporalType.DATE)
	private Date dataCadastro;

	@Column(name = "arquivo_foto")
	private String arqivoFoto;

	@Column(name = "telefone_responsavel")
	private String telefoneResponsavel;

	@Column(name = "bairro")
	private String bairro;

	@ManyToOne
	@JoinColumn(name = "unidadeCadastro")
	private Unidade unidadeCadastro;

	@Transient
	private String dataNascimentoStr;

	@Transient
	private String dataCadastroStr;

	public String getDataNascimentoStr() {
		return dataNascimento == null ? " - " : DateFormat.getDateInstance().format(dataNascimento);
	}

	public String getDataCadastroStr() {
		return dataCadastro == null ? " - " : DateFormat.getDateInstance().format(dataCadastro);
	}

	public Unidade getUnidadeCadastro() {
		return unidadeCadastro;
	}

	public void setUnidadeCadastro(Unidade unidadeCadastro) {
		this.unidadeCadastro = unidadeCadastro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public Integer getAlunoId() {
		return alunoId;
	}

	public void setAlunoId(Integer alunoId) {
		this.alunoId = alunoId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getNomePai() {
		return nomePai;
	}

	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getArqivoFoto() {
		return arqivoFoto;
	}

	public void setArqivoFoto(String arqivoFoto) {
		this.arqivoFoto = arqivoFoto;
	}

	public String getTelefoneResponsavel() {
		return telefoneResponsavel;
	}

	public void setTelefoneResponsavel(String telefoneResponsavel) {
		this.telefoneResponsavel = telefoneResponsavel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alunoId == null) ? 0 : alunoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (alunoId == null) {
			if (other.alunoId != null)
				return false;
		} else if (!alunoId.equals(other.alunoId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Aluno [alunoId=" + alunoId + ", nome=" + nome + ", cpf=" + cpf + ", email=" + email + ", nomeMae="
				+ nomeMae + ", nomePai=" + nomePai + ", endereco=" + endereco + ", cidade=" + cidade
				+ ", dataNascimento=" + dataNascimento + ", dataCadastro=" + dataCadastro + ", arqivoFoto=" + arqivoFoto
				+ ", telefoneResponsavel=" + telefoneResponsavel + "]";
	}

}
