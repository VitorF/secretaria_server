package br.com.secretaria.municipio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.secretaria.estado.Estado;

@Stateless
public class MunicipioFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public Municipio salvar(Municipio municipio) {
		manager.persist(municipio);

		return municipio;
	}
	
	public List<Municipio> buscaMunicipioPorEstado(Estado estado){
		
	TypedQuery<Municipio> query  =  manager.createQuery("SELECT m FROM "+Municipio.NAME+" m WHERE m.estado = :estado",Municipio.class);	
		query.setParameter("estado",estado);
		return query.getResultList();
	}
}
