package br.com.secretaria.municipio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.secretaria.estado.Estado;

@Entity(name = Municipio.NAME)
@Table(name = "educacao.educ_municipio")
public class Municipio implements Serializable  {

	
	private static final long serialVersionUID = 1L;
	public static final String NAME = "educ_municipio";
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "municipio_id")
	private Integer municipioId;
	@Column(name="descricao")
	private String descricao;
	@OneToOne
	@JoinColumn(name="estado_id")
	private Estado estado;
	public Integer getMunicipioId() {
		return municipioId;
	}
	public void setMunicipioId(Integer municipioId) {
		this.municipioId = municipioId;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((municipioId == null) ? 0 : municipioId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Municipio other = (Municipio) obj;
		if (municipioId == null) {
			if (other.municipioId != null)
				return false;
		} else if (!municipioId.equals(other.municipioId))
			return false;
		return true;
	}
	
	
}
