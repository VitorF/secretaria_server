package br.com.secretaria.diario;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.ma.secretaria.periodo.Periodo;
import br.com.secretaria.diarioperiodo.DiarioPeriodo;
import br.com.secretaria.turmadisciplina.TurmaDisciplina;

@Entity(name = Diario.NAME)
@Table(name = "educ_diario", schema = "educacao")
public class Diario implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "educacao_diario";
	
	
	public Diario() {
		// TODO Auto-generated constructor stub
	}
	
	
	

	public Diario(Integer diarioId, TurmaDisciplina turmaDisciplina, Integer ano) {
		super();
		this.diarioId = diarioId;
		this.turmaDisciplina = turmaDisciplina;
		this.ano = ano;
	}




	@Id
	@Column(name = "diario_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer diarioId;

	@ManyToOne
	@JoinColumn(name = "turma_disciplina_id")
	private TurmaDisciplina turmaDisciplina;

	@Column(name = "ano")
	private Integer ano;

	@OneToMany(mappedBy = "diario", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
	private List<DiarioPeriodo> periodos;

	public List<DiarioPeriodo> getPeriodos() {
		return periodos;
	}
	
	@Transient
	public String getIconStatusPrimeriroBimes() {
		Stream<DiarioPeriodo> diarioPeriodo =  periodos.stream().filter(periodo -> periodo.getPeriodo().getPeriodoId().equals(1));
		
	return 	diarioPeriodo.findAny().get().getDataEntrega() == null ? "close-32.png" :"check-32.png";
	}
	
	@Transient
	public String getIconStatusSegundoBimes() {
		Stream<DiarioPeriodo> diarioPeriodo =  periodos.stream().filter(periodo -> periodo.getPeriodo().getPeriodoId().equals(2));
		
	return 	diarioPeriodo.findAny().get().getDataEntrega() == null ? "close-32.png" :"check-32.png";
	}
	
	@Transient
	public String getIconStatusTerceiroBimes() {
		Stream<DiarioPeriodo> diarioPeriodo =  periodos.stream().filter(periodo -> periodo.getPeriodo().getPeriodoId().equals(3));
		
	return 	diarioPeriodo.findAny().get().getDataEntrega() == null ? "close-32.png" :"check-32.png";
	}
	
	@Transient
	public String getIconStatusQuartoBimes() {
		Stream<DiarioPeriodo> diarioPeriodo =  periodos.stream().filter(periodo -> periodo.getPeriodo().getPeriodoId().equals(4));
		
	return 	diarioPeriodo.findAny().get().getDataEntrega() == null ? "close-32.png" :"check-32.png";
	}

	public void setPeriodos(List<DiarioPeriodo> periodos) {
		this.periodos = periodos;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getDiarioId() {
		return diarioId;
	}

	public void setDiarioId(Integer diarioId) {
		this.diarioId = diarioId;
	}

	public TurmaDisciplina getTurmaDisciplina() {
		return turmaDisciplina;
	}

	public void setTurmaDisciplina(TurmaDisciplina turmaDisciplina) {
		this.turmaDisciplina = turmaDisciplina;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((diarioId == null) ? 0 : diarioId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Diario other = (Diario) obj;
		if (diarioId == null) {
			if (other.diarioId != null)
				return false;
		} else if (!diarioId.equals(other.diarioId))
			return false;
		return true;
	}




	@Override
	public String toString() {
		return "Diario [diarioId=" + diarioId + ", turmaDisciplina=" + turmaDisciplina + ", ano=" + ano + ", periodos="
				+ periodos + "]";
	}
	
	

}
