package br.com.secretaria.diario;

import br.com.ma.secretaria.funcionario.Funcionario;
import br.com.secretaria.disciplina.Disciplina;
import br.com.secretaria.matricula.Matricula;
import br.com.secretaria.serie.Serie;
import br.com.secretaria.turno.TurnoEnum;
import br.com.secretaria.unidade.Unidade;

public class FiltroDiario {

	public FiltroDiario() {

	}

	public FiltroDiario(Unidade unidade) {

		this.unidade = unidade;

	}

	private Funcionario professor;

	private TurnoEnum turno;

	private Serie serie;

	private Integer ano;

	private Disciplina disciplina;

	private Unidade unidade;
	
	private Matricula matricula;
	
	public Matricula getMatricula() {
		return matricula;
	}
	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}
	

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Funcionario getProfessor() {
		return professor;
	}

	public void setProfessor(Funcionario professor) {
		this.professor = professor;
	}

	public TurnoEnum getTurno() {
		return turno;
	}

	public void setTurno(TurnoEnum turno) {
		this.turno = turno;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

}
