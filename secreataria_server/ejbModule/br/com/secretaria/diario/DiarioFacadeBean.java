package br.com.secretaria.diario;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.SystemException;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import br.com.secretaria.diarioperiodo.DiarioPeriodo;
import br.com.secretaria.diarioperiodo.DiarioPeriodoFacadeBean;
import br.com.secretaria.itemdiarioperiodo.ItemDiarioPeriodo;
import br.com.secretaria.itemdiarioperiodo.ItemDiarioPeriodoFacadeBean;
import br.com.secretaria.modelodiario.ModeloDiario;
import br.com.secretaria.modeloperiododiario.ModeloPeriodoDiario;
import br.com.secretaria.turmaaluno.TurmaAluno;
import br.com.secretaria.turmaaluno.TurmaAlunoFacadeBean;
import br.com.secretaria.turmadisciplina.TurmaDisciplina;
import br.com.secretaria.turmadisciplina.TurmaDisciplinaFacadeBean;

@Stateless
public class DiarioFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	@EJB
	private ItemDiarioPeriodoFacadeBean itemDiarioPeriodoFacadeBean;

	@EJB
	private DiarioPeriodoFacadeBean diarioPeriodoFacadeBean;

	@EJB
	private TurmaDisciplinaFacadeBean turmaDisciplinaFacadeBean;

	@EJB
	private TurmaAlunoFacadeBean turmaAlunoFacadeBean;

	public Diario salvar(Diario diario) {
		manager.persist(diario);
		return diario;
	}

	@Transactional
	public void gerarDiariosPorModelo(ModeloDiario modeloDiario)
			throws IllegalStateException, SecurityException, SystemException {
		try {
			List<TurmaDisciplina> listTurmaDisciplina = turmaDisciplinaFacadeBean
					.findDisciplinasSemDiario(modeloDiario.getUnidade().getUnidadeId(), modeloDiario.getAno());

			List<TurmaAluno> turmaAlunos = turmaAlunoFacadeBean
					.findAlunosUnidade(modeloDiario.getUnidade().getUnidadeId(), modeloDiario.getAno());

			for (TurmaDisciplina turmaDisciplina : listTurmaDisciplina) {

				/***
				 * Criando um diario para cada turma Disciplina
				 */
				Diario diario = new Diario();
				diario.setTurmaDisciplina(turmaDisciplina);
				diario.setAno(modeloDiario.getAno());
				manager.persist(diario);
				/***
				 * Fim Persistindo a informação
				 * 
				 */

				for (ModeloPeriodoDiario modeloPeriodo : modeloDiario.getListPeriodos()) {

					DiarioPeriodo diarioPeriodo = new DiarioPeriodo();
					diarioPeriodo.setDataFim(modeloPeriodo.getDataFim());
					diarioPeriodo.setDataInicio(modeloPeriodo.getDataInicio());
					diarioPeriodo.setPeriodo(modeloPeriodo.getPeriodo());
					diarioPeriodo.setModeloPeriodoDiario(modeloPeriodo);
					diarioPeriodo.setDiario(diario);

					manager.persist(diarioPeriodo);

					for (TurmaAluno turmaAluno : turmaAlunos) {
						ItemDiarioPeriodo idp = new ItemDiarioPeriodo();
						idp.setTurmaAluno(turmaAluno);
						idp.setDiarioPeriodo(diarioPeriodo);

						manager.persist(idp);
					}

				}

			}

		} catch (Exception e) {

			throw e;
		}
	}

	public List<Diario> findDiarioPorModeloDiario(Integer modeloDiarioId) {
		List<Diario> diarios = new ArrayList<>();
		String sql = "select      " + "distinct(ed.diario_id ) , ed.ano ,ed.turma_disciplina_id       " + "from       "
				+ "educacao.educ_diario ed ,      " + "educacao.educ_diario_periodo edp ,      "
				+ "educacao.educ_modelo_periodo_diario empd       " + "where       "
				+ "ed.diario_id  = edp.diario_id       "
				+ "and edp.modelo_periodo_diario_id   = empd.modelo_periodo_diario_id       "
				+ "and empd.modelo_diario_id  = :modeloDiarioId";

		Query query = manager.createNativeQuery(sql);

		query.setParameter("modeloDiarioId", modeloDiarioId);

		for (Object[] obj : (List<Object[]>) query.getResultList()) {

			TurmaDisciplina turmaDisciplina = new TurmaDisciplina();
			turmaDisciplina.setTurmaDisciplinaId(new BigDecimal(obj[2].toString()).intValue());
			diarios.add(new Diario(new BigDecimal(obj[0].toString()).intValue(), turmaDisciplina,
					new BigDecimal(obj[1].toString()).intValue()));
		}

		return diarios;
	}

	public void remover(Integer diarioId) {
		manager.remove(manager.find(Diario.class, diarioId));
	}

	public List<Diario> busca(FiltroDiario filtroDiario) {
		HashMap<String, Object> param = new HashMap<>();
		StringBuffer hql = new StringBuffer();

		hql.append(" SELECT d FROM " + Diario.NAME + " d  WHERE  1=1  ");
		
		param = getParametros(hql, filtroDiario);
		
		TypedQuery<Diario> query =  manager.createQuery(hql.toString(),Diario.class);
		
		for (Entry<String, Object>  entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		return query.getResultList();
	}

	private HashMap<String, Object> getParametros(StringBuffer hql, FiltroDiario filtroDiario) {
		HashMap<String, Object> param = new HashMap<>();

		hql.append(" and d.turmaDisciplina.turma.unidade.unidadeId = :unidadeId ");
		param.put("unidadeId", filtroDiario.getUnidade().getUnidadeId());

		if (filtroDiario.getAno() != null) {
			hql.append(" and d.ano = :ano ");
			param.put("ano", filtroDiario.getAno());
		}
		
		
		if (filtroDiario.getTurno() != null) {
			hql.append(" and d.turmaDisciplina.turma.turno = :turno ");
			param.put("turno", filtroDiario.getTurno());
		}
		
		
		if (filtroDiario.getSerie() != null) {
			hql.append(" and d.turmaDisciplina.turma.serie.serieId = :serieId ");
			param.put("serieId", filtroDiario.getSerie().getSerieId());
		}
		
		if (filtroDiario.getDisciplina() != null) {
			hql.append(" and d.turmaDisciplina.turma.disciplina.disciplinaId = :disciplinaId ");
			param.put("disciplinaId", filtroDiario.getDisciplina().getDisciplinaId());
		}
		
		
		if (filtroDiario.getMatricula() != null) {
			hql.append(" and d.turmaDisciplina.matricula.matriculaId = :matriculaId ");
			param.put("matriculaId", filtroDiario.getMatricula().getMatriculaId());
		}
		
		return param;

	}

	public Diario findByPrimaryKey(int diarioId) {
		return manager.find(Diario.class,diarioId);
	}

}
