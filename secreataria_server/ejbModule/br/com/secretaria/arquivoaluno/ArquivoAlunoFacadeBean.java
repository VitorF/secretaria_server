package br.com.secretaria.arquivoaluno;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ArquivoAlunoFacadeBean {
	
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;
	
	public ArquivoAluno salvar(ArquivoAluno arquivoAluno) {
		return manager.merge(arquivoAluno);
	}
	
	public void remover(Integer arquivoAlunoId) {
		manager.remove(manager.find(ArquivoAluno.class, arquivoAlunoId));
	}
	

}
