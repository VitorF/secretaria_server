package br.com.secretaria.arquivoaluno;

import java.io.InputStream;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.secretaria.aluno.Aluno;

@Entity(name = ArquivoAluno.NAME)
@Table(name = "educ_arquivo_aluno")
public class ArquivoAluno implements Serializable {

	public ArquivoAluno(String arquivo) {
		super();
		this.arquivo = arquivo;
	}
	
	public ArquivoAluno(String arquivo,InputStream inputStream) {
		this.arquivo = arquivo;
		this.inputStream = inputStream;
	}


	private static final long serialVersionUID = 1L;

	public static final String NAME = "secretaria_arquivo_aluno";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "arquivo_aluno_id")
	private Integer arquivoAlunoId;

	@Column(name = "arquivo", nullable = false)
	private String arquivo;

	@ManyToOne
	@JoinColumn(name = "aluno_id")
	private Aluno aluno;

	@Transient
	private String tipo;

	@Transient
	private String size;
	
	@Transient
	private InputStream inputStream;
	
	
	

 

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public Integer getArquivoAlunoId() {
		return arquivoAlunoId;
	}

	public void setArquivoAlunoId(Integer arquivoAlunoId) {
		this.arquivoAlunoId = arquivoAlunoId;
	}

	public String getArquivo() {
		return arquivo;
	}

	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	
	
	
	

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arquivo == null) ? 0 : arquivo.hashCode());
		result = prime * result + ((arquivoAlunoId == null) ? 0 : arquivoAlunoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArquivoAluno other = (ArquivoAluno) obj;
		if (arquivo == null) {
			if (other.arquivo != null)
				return false;
		} else if (!arquivo.equals(other.arquivo))
			return false;
		if (arquivoAlunoId == null) {
			if (other.arquivoAlunoId != null)
				return false;
		} else if (!arquivoAlunoId.equals(other.arquivoAlunoId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ArquivoAluno [arquivoAlunoId=" + arquivoAlunoId + ", arquivo=" + arquivo + ", aluno=" + aluno
				+ ", tipo=" + tipo + ", size=" + size + ", inputStream=" + inputStream + "]";
	}
	

	

}
