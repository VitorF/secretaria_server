package br.com.secretaria.usuarioperfil;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.ma.secretaria.perfil.Perfil;
import br.com.secretaria.usuario.Usuario;

@Entity(name = UsuarioPerfil.NAME)
@Table(name="educ_usuario_perfil")
public class UsuarioPerfil implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	public static final String NAME = "eduacao_usuario_perfil";

@Id
@GeneratedValue(strategy =GenerationType.IDENTITY )
@Column(name = "usuario_perfil_id")
private Long usuarioPerfilId;
@ManyToOne
private Usuario usuario;
@ManyToOne
private Perfil perfil;
public Usuario getUsuario() {
	return usuario;
}
public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
}
public Perfil getPerfil() {
	return perfil;
}
public void setPerfil(Perfil perfil) {
	this.perfil = perfil;
}



@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((perfil == null) ? 0 : perfil.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	UsuarioPerfil other = (UsuarioPerfil) obj;
	if (perfil == null) {
		if (other.perfil != null)
			return false;
	} else if (!perfil.equals(other.perfil))
		return false;
	return true;
}





}
