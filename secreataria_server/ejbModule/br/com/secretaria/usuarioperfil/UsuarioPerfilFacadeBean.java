package br.com.secretaria.usuarioperfil;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.secretaria.usuario.Usuario;

@Stateless
public class UsuarioPerfilFacadeBean {
@PersistenceContext(unitName = "educacaoPU")
private EntityManager manager;


public UsuarioPerfil salvar(UsuarioPerfil usuarioPerfil) {
	
	manager.persist(usuarioPerfil);
	return usuarioPerfil;
}

public List<UsuarioPerfil> findAllToUser(Usuario usuario){

	TypedQuery<UsuarioPerfil> query =  manager.createQuery("SELECT uf FROM "+UsuarioPerfil.NAME+" uf WHERE uf.usuario = :usuario ",UsuarioPerfil.class);
    query.setParameter("usuario",usuario);

return query.getResultList();
}


public void remover(Integer usuarioPerfilId) {
	 manager.remove(manager.find(UsuarioPerfil.class, usuarioPerfilId));
}


}
