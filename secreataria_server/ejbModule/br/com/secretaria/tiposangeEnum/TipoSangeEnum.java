package br.com.secretaria.tiposangeEnum;

public enum TipoSangeEnum {

	A_POSITIVO("A +") , 
	A_NEGATIVO("A -") , 
	B_POSITIVO("B +"),
	AB_POSIVITO("AB +"),
	B_NEGATIVO("B -"),
	AB_NEGATIVO("AB -"),
	O_POSITIVO("O +"),
	O_NEGATIVO("O -");
	
	
	private String descricao;
	
	
	TipoSangeEnum(String descricao) {
		this.descricao = descricao;
	}
	
	
	public String getDescricao() {
		return this.descricao;
	}
}
