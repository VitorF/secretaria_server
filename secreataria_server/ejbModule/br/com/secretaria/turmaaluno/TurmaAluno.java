package br.com.secretaria.turmaaluno;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.secretaria.aluno.Aluno;
import br.com.secretaria.turma.Turma;

@Entity(name = TurmaAluno.NAME)
@Table(name = "educ_turma_aluno")
public class TurmaAluno implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "educacao_educ_turma_aluno";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "turma_aluno_id")
	private Integer turmaAlunoId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_matricula")
	private Date dataMatricula;

	@ManyToOne
	@JoinColumn(name = "turma_id")
	private Turma turma;
	
	@ManyToOne
	@JoinColumn(name="aluno_id")
	private Aluno aluno;

	public Integer getTurmaAlunoId() {
		return turmaAlunoId;
	}

	public void setTurmaAlunoId(Integer turmaAlunoId) {
		this.turmaAlunoId = turmaAlunoId;
	}

	public Date getDataMatricula() {
		return dataMatricula;
	}

	public void setDataMatricula(Date dataMatricula) {
		this.dataMatricula = dataMatricula;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((turmaAlunoId == null) ? 0 : turmaAlunoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TurmaAluno other = (TurmaAluno) obj;
		if (turmaAlunoId == null) {
			if (other.turmaAlunoId != null)
				return false;
		} else if (!turmaAlunoId.equals(other.turmaAlunoId))
			return false;
		return true;
	}
	
	
	
	

}
