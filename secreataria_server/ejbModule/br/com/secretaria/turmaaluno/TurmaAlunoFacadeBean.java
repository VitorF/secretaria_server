package br.com.secretaria.turmaaluno;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.secretaria.turma.Turma;

@Stateless
public class TurmaAlunoFacadeBean {
@PersistenceContext(unitName = "educacaoPU")
EntityManager manager;



public TurmaAluno update(TurmaAluno turmaAluno) {
	return manager.merge(turmaAluno);
}

public void remover(Integer turmaAlunoId) {
	manager.remove(manager.find(TurmaAluno.class, turmaAlunoId));
}

public TurmaAluno findByPrimaryKeky(Integer turmaAlunoId) {
	return manager.find(TurmaAluno.class, turmaAlunoId);
}


public List<TurmaAluno> findAll(){
	TypedQuery<TurmaAluno> query  =  manager.createQuery("SELECT t FROM "+TurmaAluno.NAME+" t ",TurmaAluno.class);

	return query.getResultList();
}



public List<TurmaAluno> findAlunosTurma(Integer turmaId){
	TypedQuery<TurmaAluno> query  =  manager.createQuery("SELECT t FROM "+TurmaAluno.NAME+" t WHERE t.turma.turmaId = :turmaId 	",TurmaAluno.class);
    query.setParameter("turmaId", turmaId);
	return query.getResultList();
}


public List<TurmaAluno> findAlunosUnidade(Integer unidadeId , Integer ano){
	TypedQuery<TurmaAluno> query  =  manager.createQuery("SELECT t FROM "+TurmaAluno.NAME+" t WHERE t.turma.unidade.unidadeId = :unidadeId  and t.turma.ano = :ano	",TurmaAluno.class);
    query.setParameter("unidadeId", unidadeId);
    query.setParameter("ano", ano);
	return query.getResultList();
}


public List<TurmaAluno> findVerificaMatriculaAluno(Turma turma){
	
	
	return null;
}


}
