package br.com.secretaria.titularidade;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class TitularidadeFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public Titularidade salvar(Titularidade titularidade) {
		manager.persist(titularidade);
		return titularidade;
	}

	public List<Titularidade> findAll() {
		TypedQuery<Titularidade> query = manager.createNamedQuery("titularidade.lista", Titularidade.class);
		return query.getResultList();
	}

	public void excluir(Long titularidadeId) {

		manager.remove(manager.find(Titularidade.class, titularidadeId));
	}

	public Titularidade editar(Titularidade titularidade) {
		return manager.merge(titularidade);
	}

}
