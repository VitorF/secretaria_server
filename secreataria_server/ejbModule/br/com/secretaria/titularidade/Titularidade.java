package br.com.secretaria.titularidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.secretaria.tipotitularidadeEnum.TipoTitularidadeEnum;

@Entity(name = Titularidade.NAME)
@Table(name = "educacao.educ_titularidade")
@NamedQueries({ @NamedQuery(name = "titularidade.lista", query = "SELECT t FROM " + Titularidade.NAME + " t ") })
public class Titularidade implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "educ_titularidade";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "titularidade_id")
	private Long titularidadeId;

	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_titularidade")
	@NotNull(message = "Selecione o tipo titularidade !!")
	private TipoTitularidadeEnum tipoTitularidadeEnum;

	@Column(name = "descricao")
	@NotEmpty(message = "O Campo 'Descri��o' � obrigatorio !!")
	private String descricao;

	public Long getTitularidadeId() {
		return titularidadeId;
	}

	public void setTitularidadeId(Long titularidadeId) {
		this.titularidadeId = titularidadeId;
	}

	public TipoTitularidadeEnum getTipoTitularidadeEnum() {
		return tipoTitularidadeEnum;
	}

	public void setTipoTitularidadeEnum(TipoTitularidadeEnum tipoTitularidadeEnum) {
		this.tipoTitularidadeEnum = tipoTitularidadeEnum;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

	@Override
	public String toString() {
		return "Titularidade [titularidadeId=" + titularidadeId + ", tipoTitularidadeEnum=" + tipoTitularidadeEnum
				+ ", descricao=" + descricao + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((titularidadeId == null) ? 0 : titularidadeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Titularidade other = (Titularidade) obj;
		if (titularidadeId == null) {
			if (other.titularidadeId != null)
				return false;
		} else if (!titularidadeId.equals(other.titularidadeId))
			return false;
		return true;
	}

}
