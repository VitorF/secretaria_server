package br.com.secretaria.tiponivelEnum;

public enum TipoNivelEnum {

	FUNDAMENTAL_MENOR("Fundamental Menor"), FUNDAMENTAL_MAIOR("Fundamental Maior"), INFANTIL(" Inf�ntil ");

	private String descricao;

	TipoNivelEnum(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
