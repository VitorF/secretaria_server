package br.com.secretaria.matricula;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.ma.secretaria.funcionario.Funcionario;
import br.com.secretaria.turmadisciplina.TurmaDisciplina;

@Stateless
public class MatriculaFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;
	
	public Matricula salvar(Matricula matricula) {
		manager.persist(matricula);
		return matricula;
	}

	public Matricula alterar(Matricula matricula) {
		return manager.merge(matricula);
	}

	public void excluir(Long matriculaId) {
		manager.remove(manager.find(Matricula.class, matriculaId));
	}

	public Matricula findPrimaryKey(Long matriculaId) {
		return manager.find(Matricula.class, matriculaId);
	}

	public List<Matricula> findAll() {
		TypedQuery<Matricula> query = manager.createQuery("SELECT m FROM  "+Matricula.NAME+" m  order by m.dataCadastro", Matricula.class);
		
		return query.getResultList();
	}

	
	
	public List<Matricula> findAllToFuncionario(Long funcionarioId) {
		TypedQuery<Matricula> query = manager.createQuery("SELECT m FROM  "+Matricula.NAME+" m where m.funcionario.funcionarioId = :funcionarioId order by m.dataCadastro", Matricula.class);
		query.setParameter("funcionarioId", funcionarioId);
		return query.getResultList();
	}
	
	
	
	public List<Matricula> findMatriculasAtivasUnidade(Integer unidadeId){
		
		TypedQuery<Matricula> query  =  manager.createQuery("SELECT m FROM "+Matricula.NAME+" m  WHERE m.unidade.unidadeId = :unidadeId and m.statu = true",Matricula.class);
		
		query.setParameter("unidadeId", unidadeId);
		return query.getResultList();
	}

	public List<Matricula> findMatriculasAtivasUnidadeProfessor(Integer unidadeId) {
TypedQuery<Matricula> query  =  manager.createQuery("SELECT m FROM "+Matricula.NAME+" m  WHERE m.unidade.unidadeId = :unidadeId and m.statu = true and m.cargo.cargoId = :cargoId",Matricula.class);
		
		query.setParameter("unidadeId", unidadeId);
		query.setParameter("cargoId", 1);
		return query.getResultList();		
	}
	
	
	
	/*
	 * Metodo que Retorna as Matriculas Disponiveis para Mapeamento
	 * 
	 */
	
	public List<Matricula> matriculasDisponiveisTurmaDisciplina(TurmaDisciplina turmaDisciplina){
		
		List<Matricula> matriculasDiscponiveis =  new ArrayList<>();
		String sql  =  "select      " + 
				"result_matricula.*     " + 
				"from(     " + 
				"select      " + 
				"matriculas_disponiveis.*,     " + 
				"(matriculas_disponiveis.cargo_horaria_esperada - matriculas_disponiveis.carga_horaria_cadastrada) as carga_horaria_restante     " + 
				"from (     " + 
				"select      " + 
				"em.matricula_id ,     " + 
				"em.numero_matricula  ,     " + 
				"ef.func_nome ,     " + 
				"em.cargo_horaria_esperada ,     " + 
				"(select IFNULL(sum(ed.carga_horaria ),0)     " + 
				"from educacao.turma_disciplina td ,      " + 
				"educacao.educ_turma et ,      " + 
				"educacao.educ_disciplina ed      " + 
				"where      " + 
				"td.matricula_id  = em.matricula_id      " + 
				"and ed.disciplina_id = td.disciplina_id      " + 
				"and  td.turma_id  = et.turma_id       " + 
				"and et.ano = :ano) as carga_horaria_cadastrada     " + 
				"from      " + 
				"educacao.educ_matricula em ,     " + 
				"educacao.educ_funcionario ef      " + 
				"where      " + 
				"ef.funcionario_id  =em.funcionario_id      " + 
				"and em.cargo_id  = 1     " + 
				"and em.unidade_id  = :unidade_id      " + 
				"and em.cargo_horaria_esperada >= :cargaHoraria     " + 
				") as matriculas_disponiveis     " + 
				"where      " + 
				"matriculas_disponiveis.cargo_horaria_esperada <> matriculas_disponiveis.carga_horaria_cadastrada     " + 
				") as result_matricula     " + 
				"where      " + 
				"result_matricula.carga_horaria_restante >= :cargaHoraria     " + 
				"";
		
		
		Query query  = manager.createNativeQuery(sql);
		
		query.setParameter("ano", turmaDisciplina.getTurma().getAno());
		query.setParameter("cargaHoraria", turmaDisciplina.getDisciplina().getCargaHoraria());
		query.setParameter("unidade_id", turmaDisciplina.getTurma().getUnidade().getUnidadeId());
		Matricula matricula = null;
		
		for (Object []  obj: (List<Object[]>)query.getResultList()) {
			
			matricula =  new Matricula();
			matricula.setMatriculaId(Long.parseLong(obj[0].toString()));
			matricula.setNumeroMatricula(obj[1].toString());
			
			Funcionario funcionario =  new Funcionario();
			funcionario.setNome((String)obj[2]);
			
			matricula.setFuncionario(funcionario);
			
			matricula.setCargoHorariaEsperada(Double.parseDouble(obj[3].toString()));
			matricula.setCargaHorariaDisponivel(Float.parseFloat(obj[5].toString()));
			
			matriculasDiscponiveis.add(matricula);
		}
		
		
		return matriculasDiscponiveis;
		
		
	}
	
	
	
	/*
	 * Retorna os Funcionarios com cargo de professor da unidade especificada
	 */

	
	public List<Funcionario> findProfessorToUnidade(Integer unidadeId){
		
		
		String hql  =  " SELECT distinct  m.funcionario FROM "+Matricula.NAME+" m WHERE m.unidade.unidadeId = :unidadeId and m.cargo.cargoId = 1  ";
		
		TypedQuery<Funcionario>  query  =  manager.createQuery(hql,Funcionario.class);
		
		query.setParameter("unidadeId", unidadeId);
		
		return query.getResultList();
	}
}
