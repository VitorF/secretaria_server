package br.com.secretaria.matricula;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.ma.secretaria.funcionario.Funcionario;
import br.com.secretaria.cargo.Cargo;
import br.com.secretaria.unidade.Unidade;

@Entity(name = Matricula.NAME)
@Table(name = "educacao.educ_matricula")
public class Matricula implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String NAME = "educ_matricula";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "matricula_id")
	private Long matriculaId;

	@Column(name = "observacao")
	private String observacao;

	@ManyToOne
	@JoinColumn(name = "unidade_id")
	private Unidade unidade;

	@ManyToOne
	@JoinColumn(name = "cargo_id")
	private Cargo cargo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_cadastro")
	private Date dataCadastro;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_fim")
	private Date dataFim;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
	private Date dataAltercao;

	@Column(name = "statu")
	private Boolean statu;

	@ManyToOne
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;

	@Column(name="cargo_horaria_esperada")
	private Double cargoHorariaEsperada;
	
	@Column(name="numero_matricula")
	private String numeroMatricula;
	
	
	@Transient
	private  Float cargaHorariaDisponivel;
	
	
	public Float getCargaHorariaDisponivel() {
		return cargaHorariaDisponivel;
	}
	
	public void setCargaHorariaDisponivel(Float cargaHorariaDisponivel) {
		this.cargaHorariaDisponivel = cargaHorariaDisponivel;
	}
	
	
	public String getNumeroMatricula() {
		return numeroMatricula;
	}

	public void setNumeroMatricula(String numeroMatricula) {
		this.numeroMatricula = numeroMatricula;
	}

	public Double getCargoHorariaEsperada() {
		return cargoHorariaEsperada;
	}

	public void setCargoHorariaEsperada(Double cargoHorariaEsperada) {
		this.cargoHorariaEsperada = cargoHorariaEsperada;
	}

	public Long getMatriculaId() {
		return matriculaId;
	}

	public void setMatriculaId(Long matriculaId) {
		this.matriculaId = matriculaId;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Date getDataAltercao() {
		return dataAltercao;
	}

	public void setDataAltercao(Date dataAltercao) {
		this.dataAltercao = dataAltercao;
	}

	public Boolean getStatu() {
		return statu;
	}

	public void setStatu(Boolean statu) {
		this.statu = statu;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	
	
	@Transient
	private String getIconAtivo() {
		return statu ? "ui-icon-check"  : "	ui-icon-close";
	}
	
	
	@Transient
	public boolean isAcessoDirecao() {
		
		if(cargo.getCargoId().equals(2) || cargo.getCargoId().equals(3)  ) {
			return true;
		}
		
		return false;
	}
	
	@Transient
	private String getDataCadastroStr() {
		return  DateFormat.getDateInstance().format(dataCadastro);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((matriculaId == null) ? 0 : matriculaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Matricula other = (Matricula) obj;
		if (matriculaId == null) {
			if (other.matriculaId != null)
				return false;
		} else if (!matriculaId.equals(other.matriculaId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Matricula [matriculaId=" + matriculaId + ", observacao=" + observacao + ", unidade=" + unidade
				+ ", cargo=" + cargo + ", dataCadastro=" + dataCadastro + ", dataFim=" + dataFim + ", dataAltercao="
				+ dataAltercao + ", statu=" + statu + ", funcionario=" + funcionario + ", cargoHorariaEsperada="
				+ cargoHorariaEsperada + ", numeroMatricula=" + numeroMatricula + "]";
	}

	
	
	
	
}
