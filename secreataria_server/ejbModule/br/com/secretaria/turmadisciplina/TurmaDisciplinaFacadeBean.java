package br.com.secretaria.turmadisciplina;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.secretaria.disciplina.Disciplina;
import br.com.secretaria.matricula.Matricula;
import br.com.secretaria.turma.Turma;

@Stateless
public class TurmaDisciplinaFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public TurmaDisciplina salvar(TurmaDisciplina turmaDisciplina) {
		manager.persist(turmaDisciplina);

		return turmaDisciplina;
	}

	public void remover(Integer turmaDisciplinaId) {
		manager.remove(manager.getReference(TurmaDisciplina.class, turmaDisciplinaId));
	}

	public TurmaDisciplina findByPrimaryKey(Integer turmaDisciplinaId) {
		return manager.find(TurmaDisciplina.class, turmaDisciplinaId);
	}

	public TurmaDisciplina update(TurmaDisciplina turmaDisciplina) {
		return manager.merge(turmaDisciplina);
	}

	public List<TurmaDisciplina> findAll() {
		TypedQuery<TurmaDisciplina> query = manager.createQuery("SELECT d FROM " + TurmaDisciplina.NAME + " d",
				TurmaDisciplina.class);
		return query.getResultList();
	}

	public List<TurmaDisciplina> findAllUnidade(Integer unidadeId, Integer ano) {
		TypedQuery<TurmaDisciplina> query = manager.createQuery(
				"SELECT d FROM " + TurmaDisciplina.NAME
						+ " d WHERE d.turma.unidade.unidadeId = :unidadeId and d.turma.ano = :ano ",
				TurmaDisciplina.class);

		query.setParameter("unidadeId", unidadeId);
		query.setParameter("ano", ano);

		return query.getResultList();
	}

	public List<TurmaDisciplina> findDisciplinasSemDiario(Integer unidadeId, Integer ano) {

		List<TurmaDisciplina> listTurmaDisciplina = new ArrayList<>();

		String sql = "select     " + "td.turma_disciplina_id  , td.turma_id  , td.disciplina_id  , td.matricula_id   "
				+ "from     " + "educacao.turma_disciplina  td    " + "left join educacao.educ_diario  ed    "
				+ "on td.turma_disciplina_id  = ed.turma_disciplina_id     " + "inner join educacao.educ_turma  et    "
				+ "on et.turma_id  = td.turma_id    " + "where     " + "et.ano  = :ano    "
				+ "and et.unidade_unidade_id  = :unidadeId    " + "and ed.diario_id  is  null";

		Query query = manager.createNativeQuery(sql);
		query.setParameter("unidadeId", unidadeId);
		query.setParameter("ano", ano);

		List<Object[]> listObject = (List<Object[]>) query.getResultList();
		TurmaDisciplina turmaDisciplina = null;
		for (Object[] obj : listObject) {
			turmaDisciplina = new TurmaDisciplina();
			turmaDisciplina.setTurmaDisciplinaId(new BigDecimal(obj[0].toString()).intValue());

			Turma turma = new Turma();
			turma.setTurmaId(new BigDecimal(obj[1].toString()).intValue());

			Disciplina disciplina = new Disciplina();
			disciplina.setDisciplinaId(new BigDecimal(obj[2].toString()).intValue());

			Matricula matricula = new Matricula();
			matricula.setMatriculaId(obj[3] == null ? null : new BigDecimal(obj[3].toString()).longValue());

			turmaDisciplina.setTurma(turma);
			turmaDisciplina.setMatricula(matricula);
			turmaDisciplina.setDisciplina(disciplina);

			listTurmaDisciplina.add(turmaDisciplina);

		}

		return listTurmaDisciplina;
	}

	public List<TurmaDisciplina> filtro(FiltroTurmaDisciplina filtroTurmaDisciplina) {
		HashMap<String, Object> parans = new HashMap<String, Object>();
		StringBuffer hql = new StringBuffer();

		hql.append("SELECT td FROM " + TurmaDisciplina.NAME + " td WHERE 1=1 ");

		parans = preencheParamentro(hql, filtroTurmaDisciplina);

		TypedQuery<TurmaDisciplina> query = manager.createQuery(hql.toString(), TurmaDisciplina.class);

		for (Entry<String, Object> entry : parans.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		return query.getResultList();
	}

	public List<TurmaDisciplina> findAllTurma(Integer turmaId) {
		TypedQuery<TurmaDisciplina> query = manager.createQuery(
				"SELECT d FROM " + TurmaDisciplina.NAME + " d WHERE d.turma.turmaId = :turmaId", TurmaDisciplina.class);
		query.setParameter("turmaId", turmaId);
		return query.getResultList();
	}

	public boolean isPossuiDisciplina(Integer turmaId, Integer disciplinaId) {
		TypedQuery<TurmaDisciplina> query = manager.createQuery(
				"SELECT d FROM " + TurmaDisciplina.NAME
						+ " d WHERE d.turma.turmaId = :turmaId and d.disciplina.disciplinaId = :disciplinaId",
				TurmaDisciplina.class);
		query.setParameter("turmaId", turmaId);
		query.setParameter("disciplinaId", disciplinaId);
		return query.getResultList().size() > 0 ? true : false;

	}

	private HashMap<String, Object> preencheParamentro(StringBuffer hql, FiltroTurmaDisciplina filtroTurmaDisciplina) {
		HashMap<String, Object> parans = new HashMap<String, Object>();

		if (filtroTurmaDisciplina.getSerie() != null) {
			hql.append(" and  td.turma.serie.serieId = :serieId ");
			parans.put("serieId", filtroTurmaDisciplina.getSerie().getSerieId());
		}

		if (filtroTurmaDisciplina.getDisciplina() != null) {
			hql.append(" and  td.disciplina.disciplinaId = :disciplinaId ");
			parans.put("disciplinaId", filtroTurmaDisciplina.getDisciplina().getDisciplinaId());
		}

		if (filtroTurmaDisciplina.getMatricula() != null) {
			hql.append(" and  td.matricula.matriculaId = :matriculaId ");
			parans.put("matriculaId", filtroTurmaDisciplina.getMatricula().getMatriculaId());
		}

		if (filtroTurmaDisciplina.getAno() != null) {
			hql.append(" and  td.turma.ano = :ano ");
			parans.put("ano", filtroTurmaDisciplina.getAno());
		}

		return parans;
	}

}
