package br.com.secretaria.turmadisciplina;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.secretaria.disciplina.Disciplina;
import br.com.secretaria.matricula.Matricula;
import br.com.secretaria.turma.Turma;
@Entity(name=TurmaDisciplina.NAME)
@Table(name="turma_disciplina")
public class TurmaDisciplina implements Serializable   {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String NAME = "educ_turma_disciplina";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="turma_disciplina_id")
	private Integer turmaDisciplinaId;
	
	@ManyToOne
	@JoinColumn(name="turma_id")
	private Turma turma;
	
	@ManyToOne
	@JoinColumn(name="disciplina_id")
	private Disciplina disciplina;
	
	@ManyToOne
	@JoinColumn(name="matricula_id")
	private Matricula matricula;
	
	
	@Transient
	private String nomeProfessor;
	
	
	public String getNomeProfessor() {
	
		return matricula == null ? " Sem Professor Mapeado" : matricula.getFuncionario().getNome();
	}
	

	public Integer getTurmaDisciplinaId() {
		return turmaDisciplinaId;
	}

	public void setTurmaDisciplinaId(Integer turmaDisciplinaId) {
		this.turmaDisciplinaId = turmaDisciplinaId;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

	public Matricula getMatricula() {
		return matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((turmaDisciplinaId == null) ? 0 : turmaDisciplinaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TurmaDisciplina other = (TurmaDisciplina) obj;
		if (turmaDisciplinaId == null) {
			if (other.turmaDisciplinaId != null)
				return false;
		} else if (!turmaDisciplinaId.equals(other.turmaDisciplinaId))
			return false;
		return true;
	}
	
	
	
    

}
