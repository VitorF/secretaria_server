package br.com.secretaria.turmadisciplina;

import br.com.secretaria.disciplina.Disciplina;
import br.com.secretaria.matricula.Matricula;
import br.com.secretaria.serie.Serie;

public class FiltroTurmaDisciplina {
	
	
	
	private Serie serie;
	
	private Matricula matricula;
	
	private Disciplina disciplina;
	
	private Integer ano;
	
	

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public Matricula getMatricula() {
		return matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}
	
	
	
	
	

}
