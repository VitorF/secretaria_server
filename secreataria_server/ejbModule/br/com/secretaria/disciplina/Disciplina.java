package br.com.secretaria.disciplina;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.secretaria.tiponivelEnum.TipoNivelEnum;

@Entity(name = Disciplina.NAME)
@Table(name="educ_disciplina")
public class Disciplina implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "educacao_disciplina";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "disciplina_id")
	private Integer disciplinaId;

	@Column(name = "descricao")
	private String descricao;

	@Column(name="carga_horaria")
	private Float cargaHoraria;
	
	@Column(name = "tipo_nivel")
	@Enumerated(EnumType.STRING)
	private TipoNivelEnum tipoNivelEnum;
	
	
	
	

	public Integer getDisciplinaId() {
		return disciplinaId;
	}

	public void setDisciplinaId(Integer disciplinaId) {
		this.disciplinaId = disciplinaId;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoNivelEnum getTipoNivelEnum() {
		return tipoNivelEnum;
	}

	public void setTipoNivelEnum(TipoNivelEnum tipoNivelEnum) {
		this.tipoNivelEnum = tipoNivelEnum;
	}
	
	

	public Float getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(Float cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((disciplinaId == null) ? 0 : disciplinaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Disciplina other = (Disciplina) obj;
		if (disciplinaId == null) {
			if (other.disciplinaId != null)
				return false;
		} else if (!disciplinaId.equals(other.disciplinaId))
			return false;
		return true;
	}
	
	
	
	
	

}
