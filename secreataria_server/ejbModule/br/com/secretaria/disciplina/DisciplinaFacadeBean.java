package br.com.secretaria.disciplina;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.secretaria.tiponivelEnum.TipoNivelEnum;

@Stateless
public class DisciplinaFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public Disciplina salvar(Disciplina disciplina) {
		manager.persist(disciplina);

		return disciplina;
	}
	
	
	public void remover(Integer disciplinaId) {
		manager.remove(manager.find(Disciplina.class, disciplinaId));
	}
	
	
	public Disciplina findByPrimaryKey(Integer disciplinaId) {
		return manager.find(Disciplina.class, disciplinaId);
	}
	
	public Disciplina update(Disciplina disciplina) {
		return manager.merge(disciplina);
	}

	public List<Disciplina> findAll() {
		TypedQuery<Disciplina> query = manager.createQuery("SELECT d FROM " + Disciplina.NAME + " d", Disciplina.class);
		return query.getResultList();
	}
	
	
	public List<Disciplina> findToTipoNilvel(TipoNivelEnum tipoNivelEnum) {
		TypedQuery<Disciplina> query = manager.createQuery("SELECT d FROM " + Disciplina.NAME + " d  where d.tipoNivelEnum = :tipoNivel", Disciplina.class);
		query.setParameter("tipoNivel",tipoNivelEnum);
		return query.getResultList();
	}

}
