package br.com.secretaria.unidade;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class UnidadeFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public Unidade salvar(Unidade unidade) {
		manager.persist(unidade);
		return unidade;
	}

	public Unidade alterar(Unidade unidade) {
		return manager.merge(unidade);
	}

	public void excluir(Integer unidadeId) {
		manager.remove(manager.find(Unidade.class, unidadeId));
	}

	public Unidade findByPrimaryKey(Integer unidadeId) {
		return manager.find(Unidade.class, unidadeId);
	}

	public List<Unidade> findAll() {
		TypedQuery<Unidade> query = manager.createQuery("SELECT u  FROM " + Unidade.NAME + " u", Unidade.class);
		return query.getResultList();

	}
}
