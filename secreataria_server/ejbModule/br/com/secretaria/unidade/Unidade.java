package br.com.secretaria.unidade;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity(name = Unidade.NAME)
@Table(name = "educacao.educ_unidade")
public class Unidade implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String NAME = "educ_unidade";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "unidade_id")
	private Integer unidadeId;
	@NotEmpty(message = "descricao da unidade obrigatória !")
	@Column(name = "descricao")
	private String descricao;
	@Column(name = "endereco")
	private String endereco;
	@Column(name = "barrio")
	private String barrio;
	@Column(name = "telefone")
	private String telefone;
	@Email(message = "email inavalido !!")
	@Column(name = "email")
	private String email;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Date dataCadastro;
	@Column(name="sigla")
	private String sigla;
	
	@Transient
	public String dataCadastroStr() {
		SimpleDateFormat sdf =  new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(this.dataCadastro);
	}
	
	
	
	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public Integer getUnidadeId() {
		return unidadeId;
	}
	public void setUnidadeId(Integer unidadeId) {
		this.unidadeId = unidadeId;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getBarrio() {
		return barrio;
	}
	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Unidade [unidadeId=" + unidadeId + ", descricao=" + descricao + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((unidadeId == null) ? 0 : unidadeId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Unidade other = (Unidade) obj;
		if (unidadeId == null) {
			if (other.unidadeId != null)
				return false;
		} else if (!unidadeId.equals(other.unidadeId))
			return false;
		return true;
	}

	
	
	
}
