package br.com.secretaria.turma;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class TurmaFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;
	

	public Turma update(Turma turma) {
		return manager.merge(turma);
	}

	public void remover(Integer turmaId) {
		manager.remove(manager.find(Turma.class, turmaId));
	}

	public Turma findByPrimaryKeky(Integer turmaId) {
		return manager.find(Turma.class, turmaId);
	}
	
	
	public List<Turma> findAll(){
		TypedQuery<Turma> query  =  manager.createQuery("SELECT t FROM "+Turma.NAME+" t ",Turma.class);
	
		return query.getResultList();
	}
	
	
	public List<Turma> buscaPorUnidade(Integer unidadeId){
		TypedQuery<Turma> query  =  manager.createQuery("SELECT t FROM "+Turma.NAME+" t  where t.unidade.unidadeId = :unidadeId",Turma.class);
	
		
		query.setParameter("unidadeId", unidadeId);
		
		return query.getResultList();
	}

}
