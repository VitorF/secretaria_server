package br.com.secretaria.turma;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.com.secretaria.serie.Serie;
import br.com.secretaria.turmadisciplina.TurmaDisciplina;
import br.com.secretaria.turno.TurnoEnum;
import br.com.secretaria.unidade.Unidade;

@Entity(name = Turma.NAME)
@Table(name = "educ_turma")
public class Turma implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String NAME = "educacao_turma";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "turma_id")
	private Integer turmaId;
	
	@Column(name = "descricao")
	@NotNull(message = "Informe a descri��o da Turma ")
	private String descricao;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "turno")
	private TurnoEnum turno;
	
	@Column(name="ano")
	private Integer ano ;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_criacao")
	private Date dataCriacao;
	
	@Column(name="ativo")
	private boolean ativo;
	
	@ManyToOne
	private Unidade unidade;
	
	@ManyToOne
	private  Serie serie;
	
	
	@OneToMany(mappedBy = "turma",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	 List<TurmaDisciplina> turmaDisciplinas;
	
    
	
	
	
	public Integer getTurmaId() {
		return turmaId;
	}

	public void setTurmaId(Integer turmaId) {
		this.turmaId = turmaId;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TurnoEnum getTurno() {
		return turno;
	}

	public void setTurno(TurnoEnum turno) {
		this.turno = turno;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((turmaId == null) ? 0 : turmaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Turma other = (Turma) obj;
		if (turmaId == null) {
			if (other.turmaId != null)
				return false;
		} else if (!turmaId.equals(other.turmaId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Turma [turmaId=" + turmaId + ", descricao=" + descricao + ", turno=" + turno + ", ano=" + ano
				+ ", dataCriacao=" + dataCriacao + ", ativo=" + ativo + ", unidade=" + unidade + ", serie=" + serie
				+ ", turmaDisciplinas=" + turmaDisciplinas + "]";
	}
	 
	
	
	
	
	

}
