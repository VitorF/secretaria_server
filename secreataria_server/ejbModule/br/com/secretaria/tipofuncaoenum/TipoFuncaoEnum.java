package br.com.secretaria.tipofuncaoenum;

public enum TipoFuncaoEnum {
	
PROFESSOR("Professor"),
DIRETOR("Diretor"),
SECRETARIO("Secretario"),
OUTRO("Outro");
	
	private String descricao;
	
	 TipoFuncaoEnum(String descricao) {
		this.descricao = descricao;
	}
	 
	 public String getDescricao() {
		return descricao;
	}

}
