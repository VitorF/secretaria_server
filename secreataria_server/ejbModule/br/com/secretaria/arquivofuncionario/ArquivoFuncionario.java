package br.com.secretaria.arquivofuncionario;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.ma.secretaria.funcionario.Funcionario;

@Entity(name = ArquivoFuncionario.NAME)
@Table(name = "educacao.educ_arquivo_funcionario")
public class ArquivoFuncionario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NAME = "educacao_arquivo_funcionario";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "arquivo_id")
	private Long arquivoId;
	@Column(name = "arquivo")
	private String arquivo;
	@ManyToOne
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;
	public Long getArquivoId() {
		return arquivoId;
	}
	public void setArquivoId(Long arquivoId) {
		this.arquivoId = arquivoId;
	}
	public String getArquivo() {
		return arquivo;
	}
	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arquivoId == null) ? 0 : arquivoId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArquivoFuncionario other = (ArquivoFuncionario) obj;
		if (arquivoId == null) {
			if (other.arquivoId != null)
				return false;
		} else if (!arquivoId.equals(other.arquivoId))
			return false;
		return true;
	}
	
	
	
}
