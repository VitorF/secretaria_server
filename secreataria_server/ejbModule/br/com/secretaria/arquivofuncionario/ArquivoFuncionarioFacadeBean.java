package br.com.secretaria.arquivofuncionario;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import br.com.ma.secretaria.funcionario.Funcionario;
@Stateless
public class ArquivoFuncionarioFacadeBean {

	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public ArquivoFuncionario salvar(ArquivoFuncionario arquivoFuncionario) {
		manager.persist(arquivoFuncionario);
		return arquivoFuncionario;
	}

	public List<ArquivoFuncionario> buscaArquivoPorFuncionario(Funcionario funcionario) {
		TypedQuery<ArquivoFuncionario> query = manager.createQuery(
				"SELECT a FROM " + ArquivoFuncionario.NAME + " a WHERE a.funcionario = :funcionario",
				ArquivoFuncionario.class);
		query.setParameter("funcionario", funcionario);
		return query.getResultList();
	}

	public void excluir(ArquivoFuncionario arquivoFuncionario) {
		manager.remove(manager.find(ArquivoFuncionario.class, arquivoFuncionario.getArquivoId()));
	}
}
