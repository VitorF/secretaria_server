package br.com.secretaria.itemdiarioperiodo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.secretaria.diarioperiodo.DiarioPeriodo;
import br.com.secretaria.turmaaluno.TurmaAluno;
@Entity(name = ItemDiarioPeriodo.NAME)
@Table(name = "educ_item_diario_periodo", schema = "educacao")
public class ItemDiarioPeriodo implements Serializable {


	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "educacao_item_diario_periodo";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "item_diario_periodo_id")
	private Integer itemDiarioPeriodoId;

	@ManyToOne
	@JoinColumn(name="turma_aluno_id")
	private TurmaAluno turmaAluno;

	@ManyToOne
	@JoinColumn(name="diario_periodo_id")
	private DiarioPeriodo diarioPeriodo;

	@Column(name="nota1")
	private BigDecimal nota1;

	@Column(name="nota2")
	private BigDecimal nota2;

	@Column(name="nota3")
	private BigDecimal nota3;
	
	@Column(name="nota_recuperacao")
	private BigDecimal notaRecuperacao;
	
	@Column(name="total_falta")
	private Integer totalFalta;
	
	@Column(name = "observacao")
	private String observacao;
	
	@Column(name="ultima_atualizacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Date  utimaAtualizacao;

	@Transient
	private BigDecimal media;
	
	
	


	public Date getUtimaAtualizacao() {
		return utimaAtualizacao;
	}

	public void setUtimaAtualizacao(Date utimaAtualizacao) {
		this.utimaAtualizacao = utimaAtualizacao;
	}

	public BigDecimal getNotaRecuperacao() {
		return notaRecuperacao;
	}

	public void setNotaRecuperacao(BigDecimal notaRecuperacao) {
		this.notaRecuperacao = notaRecuperacao;
	}

	public Integer getTotalFalta() {
		return totalFalta;
	}

	public void setTotalFalta(Integer totalFalta) {
		this.totalFalta = totalFalta;
	}

	public Integer getItemDiarioPeriodoId() {
		return itemDiarioPeriodoId;
	}

	public void setItemDiarioPeriodoId(Integer itemDiarioPeriodoId) {
		this.itemDiarioPeriodoId = itemDiarioPeriodoId;
	}

	public TurmaAluno getTurmaAluno() {
		return turmaAluno;
	}

	public void setTurmaAluno(TurmaAluno turmaAluno) {
		this.turmaAluno = turmaAluno;
	}

	public DiarioPeriodo getDiarioPeriodo() {
		return diarioPeriodo;
	}

	public void setDiarioPeriodo(DiarioPeriodo diarioPeriodo) {
		this.diarioPeriodo = diarioPeriodo;
	}

	public BigDecimal getNota1() {
		return nota1;
	}

	public void setNota1(BigDecimal nota1) {
		this.nota1 = nota1;
	}

	public BigDecimal getNota2() {
		return nota2;
	}

	public void setNota2(BigDecimal nota2) {
		this.nota2 = nota2;
	}

	public BigDecimal getNota3() {
		return nota3;
	}

	public void setNota3(BigDecimal nota3) {
		this.nota3 = nota3;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public BigDecimal getMedia() {
		return media;
	}

	public void setMedia(BigDecimal media) {
		this.media = media;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemDiarioPeriodoId == null) ? 0 : itemDiarioPeriodoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemDiarioPeriodo other = (ItemDiarioPeriodo) obj;
		if (itemDiarioPeriodoId == null) {
			if (other.itemDiarioPeriodoId != null)
				return false;
		} else if (!itemDiarioPeriodoId.equals(other.itemDiarioPeriodoId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ItemDiarioPeriodo [itemDiarioPeriodoId=" + itemDiarioPeriodoId + ", turmaAluno=" + turmaAluno
				+ ", diarioPeriodo=" + diarioPeriodo + ", nota1=" + nota1 + ", nota2=" + nota2 + ", nota3=" + nota3
				+ ", observacao=" + observacao + ", media=" + media + ", getItemDiarioPeriodoId()="
				+ getItemDiarioPeriodoId() + ", getTurmaAluno()=" + getTurmaAluno() + ", getDiarioPeriodo()="
				+ getDiarioPeriodo() + ", getNota1()=" + getNota1() + ", getNota2()=" + getNota2() + ", getNota3()="
				+ getNota3() + ", getObservacao()=" + getObservacao() + ", getMedia()=" + getMedia() + ", hashCode()="
				+ hashCode() + ", getClass()=" + getClass() + ", toString()=" + super.toString() + "]";
	}

	
	
	
	

}
