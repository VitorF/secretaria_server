package br.com.secretaria.itemdiarioperiodo;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.secretaria.diarioperiodo.DiarioPeriodo;

@Stateless
public class ItemDiarioPeriodoFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public ItemDiarioPeriodo salvar(ItemDiarioPeriodo itemDiarioPeriodo) {
		manager.persist(itemDiarioPeriodo);
		return itemDiarioPeriodo;
	}
	
	
	

	public ItemDiarioPeriodo alterar(ItemDiarioPeriodo itemDiarioPeriodo) {
		return manager.merge(itemDiarioPeriodo);
	}
	
	
	
	
	public void remover(Integer itemDiarioPeriodoId) {
	    manager.remove(manager.find(ItemDiarioPeriodo.class, itemDiarioPeriodoId));	
	}
	
	
	
	
	public List<ItemDiarioPeriodo> findAll(){
		
		
		return null;
	}
}
