package br.com.secretaria.estado;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class EstadoFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public Estado salvar(Estado estado) {
		manager.persist(estado);
		return estado;
	}

	public List<Estado> findAll() {
		TypedQuery<Estado> query = manager.createNamedQuery("estado.lista", Estado.class);
		return query.getResultList();
	}

}
