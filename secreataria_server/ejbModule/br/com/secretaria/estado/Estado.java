package br.com.secretaria.estado;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity(name = Estado.NAME)
@Table(name = "educacao.educ_estado")

@NamedQueries({@NamedQuery(name="estado.lista",query="SELECT e FROM "+Estado.NAME+" e")})
public class Estado implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "educ_estado";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="estado_id")
	private Integer estadoId;

	@Column(name="descricao")
	private String descricao;

	@Column(name="sigla")
	private String sigla;
	
	
	
	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Integer getEstadoId() {
		return estadoId;
	}

	public void setEstadoId(Integer estadoId) {
		this.estadoId = estadoId;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estadoId == null) ? 0 : estadoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estado other = (Estado) obj;
		if (estadoId == null) {
			if (other.estadoId != null)
				return false;
		} else if (!estadoId.equals(other.estadoId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Estado [estadoId=" + estadoId + ", descricao=" + descricao + ", sigla=" + sigla + "]";
	}
	
	
	
}
