package br.com.secretaria.modelodiario;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.secretaria.diario.Diario;
import br.com.secretaria.diario.DiarioFacadeBean;
import br.com.secretaria.diarioperiodo.DiarioPeriodoFacadeBean;
import br.com.secretaria.modeloperiododiario.ModeloPeriodoDiario;

@Stateless
public class ModeloDiarioFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;
	
	@EJB
	private DiarioFacadeBean diarioFacadeBean;
	@EJB
	private DiarioPeriodoFacadeBean diarioPeriodoFacadeBean;

	public ModeloDiario salvar(ModeloDiario modeloDiario) {

		manager.persist(modeloDiario);

		return modeloDiario;
	}

	public List<ModeloDiario> finAll() {

		return null;
	}

	public List<ModeloDiario> finAllToUnidade(Integer unidadeId) {

		TypedQuery<ModeloDiario> query = manager.createQuery(
				"SELECT m FROM " + ModeloDiario.NAME + " m WHERE m.unidade.unidadeId = :unidadeId ",
				ModeloDiario.class);

		query.setParameter("unidadeId", unidadeId);
		return query.getResultList();
	}

	public void remove(Integer modeloDiarioId) {
		manager.remove(manager.find(ModeloDiario.class, modeloDiarioId));
	}

	public void removeFull(Integer modeloDiarioId) {
		ModeloDiario modeloDiario = findByPrimaryKey(modeloDiarioId);

		/*
		 * Removendo Todos os diarios e seu relacionamentos desse modelo
		 */
		for (Diario diario : diarioFacadeBean.findDiarioPorModeloDiario(modeloDiarioId)) {
			System.out.println(diario);
			diarioFacadeBean.remover(diario.getDiarioId());
		}
		
		remove(modeloDiarioId);

	}
	
	
	
	
	
	

	public ModeloDiario findByPrimaryKey(Integer modeloDiarioId) {
		return manager.find(ModeloDiario.class, modeloDiarioId);
	}

}
