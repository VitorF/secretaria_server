package br.com.secretaria.modelodiario;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import br.com.ma.secretaria.periodo.Periodo;
import br.com.secretaria.modeloperiododiario.ModeloPeriodoDiario;
import br.com.secretaria.unidade.Unidade;
import br.com.secretaria.usuario.Usuario;

@Entity(name = ModeloDiario.NAME)
@Table(name = "educ_modelo_diario", schema = "educacao")
public class ModeloDiario implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "educacao_modelo_diario";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "modelo_diario_id")
	private Integer modeloDiarioId;

	@Column(name = "ano")
	private Integer ano;

	@ManyToOne
	@JoinColumn(name = "unidade_id")
	private Unidade unidade;

	@OneToMany(mappedBy = "modeloDiario", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	private List<ModeloPeriodoDiario> listPeriodos;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_criacao")
	private Date dataCriacao;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "usuario_criacao_id")
	private Usuario usuarioCriacao;

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Usuario getUsuarioCriacao() {
		return usuarioCriacao;
	}

	public void setUsuarioCriacao(Usuario usuarioCriacao) {
		this.usuarioCriacao = usuarioCriacao;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public List<ModeloPeriodoDiario> getListPeriodos() {
		return listPeriodos;
	}

	public void setListPeriodos(List<ModeloPeriodoDiario> listPeriodos) {
		this.listPeriodos = listPeriodos;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Integer getModeloDiarioId() {
		return modeloDiarioId;
	}

	public void setModeloDiarioId(Integer modeloDiarioId) {
		this.modeloDiarioId = modeloDiarioId;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((modeloDiarioId == null) ? 0 : modeloDiarioId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModeloDiario other = (ModeloDiario) obj;
		if (modeloDiarioId == null) {
			if (other.modeloDiarioId != null)
				return false;
		} else if (!modeloDiarioId.equals(other.modeloDiarioId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ModeloDiario [modeloDiarioId=" + modeloDiarioId + ", ano=" + ano + ", unidade=" + unidade
				+ ", listPeriodos=" + listPeriodos + ", dataCriacao=" + dataCriacao + ", usuarioCriacao="
				+ usuarioCriacao + "]";
	}



}
