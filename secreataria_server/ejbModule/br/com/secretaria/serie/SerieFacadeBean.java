package br.com.secretaria.serie;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.secretaria.matricula.Matricula;

@Stateless
public class SerieFacadeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;
	
	public Serie salvar(Serie serie) {
		manager.persist(serie);
		return serie;
	}

	public Serie alterar(Serie serie) {
		return manager.merge(serie);
	}

	public void excluir(Long serieId) {
		manager.remove(manager.find(Serie.class, serieId));
	}

	public Serie findPrimaryKey(Long serieId) {
		return manager.find(Serie.class, serieId);
	}

	public List<Serie> findAll() {
		TypedQuery<Serie> query = manager.createQuery("SELECT m FROM  "+Serie.NAME+" m ", Serie.class);
		
		return query.getResultList();
	}

}
