package br.com.secretaria.serie;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.secretaria.tiponivelEnum.TipoNivelEnum;

@Entity(name = Serie.NAME)
@Table(name = "educ_serie")
public class Serie implements Serializable {


	private static final long serialVersionUID = 1L;

	public static final String NAME = "educacao_serie";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "serie_id")
	private Integer serieId;

	@Column(name = "descricao")
	private String descricao;
	
	
	@Column(name = "nivel")
	@Enumerated(EnumType.STRING)
	private TipoNivelEnum tipoNivelEnum;
	
	
	public TipoNivelEnum getTipoNivelEnum() {
		return tipoNivelEnum;
	}
	
	public void setTipoNivelEnum(TipoNivelEnum tipoNivelEnum) {
		this.tipoNivelEnum = tipoNivelEnum;
	}

	public Integer getSerieId() {
		return serieId;
	}

	public void setSerieId(Integer serieId) {
		this.serieId = serieId;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((serieId == null) ? 0 : serieId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Serie other = (Serie) obj;
		if (serieId == null) {
			if (other.serieId != null)
				return false;
		} else if (!serieId.equals(other.serieId))
			return false;
		return true;
	}
	
	
	
	

}
