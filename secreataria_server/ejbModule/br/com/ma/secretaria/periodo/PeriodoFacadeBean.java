package br.com.ma.secretaria.periodo;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.secretaria.aluno.Aluno;

@Stateless
public class PeriodoFacadeBean {
	
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;
	
	
	public List<Periodo> findAll(){
	TypedQuery<Periodo> query  =  manager.createQuery("SELECT p FROM "+Periodo.NAME+" p ",Periodo.class);
	
	return query.getResultList();
	}
	
	
	
}
