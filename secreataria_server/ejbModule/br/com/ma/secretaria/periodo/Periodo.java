package br.com.ma.secretaria.periodo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = Periodo.NAME)
@Table(name = "educ_periodo", schema = "educacao")
public class Periodo implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String NAME = "educacao_educ_periodo";

	@Id
	@Column(name = "periodo_id")
	private Integer periodoId;

	@Column(name = "descricao")
	private String descricao;
	
	

	public Integer getPeriodoId() {
		return periodoId;
	}

	public void setPeriodoId(Integer periodoId) {
		this.periodoId = periodoId;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	

}
