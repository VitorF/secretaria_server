package br.com.ma.secretaria.funcionario;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class FuncionarioFacabeBean {
	@PersistenceContext(unitName = "educacaoPU")
	private EntityManager manager;

	public Funcionario salvar(Funcionario funcionario) {
		manager.persist(funcionario);
		return funcionario;
	}

	public void excluir(Long funcionarioID) {
		manager.remove(manager.find(Funcionario.class, funcionarioID));
	}

	public List<Funcionario> findAll() {
		TypedQuery<Funcionario> query = manager.createNamedQuery("funcionario.lista", Funcionario.class);
		return query.getResultList();
	}
	
	
	public Funcionario buscaPorCpf(String cpf) {
		try {
		TypedQuery<Funcionario> query = manager.createQuery("SELECT f FROM "+Funcionario.NAME+" f WHERE f.cpf = :cpf",Funcionario.class);
		query.setParameter("cpf", cpf);
		return query.getSingleResult();
		}catch(NoResultException e) {
			return null;
		}
	}

	public Funcionario alterar(Funcionario funcionario) {
		return manager.merge(funcionario);
	}

	public Funcionario findByPrimaryKey(Long funcionarioId) {

		return manager.find(Funcionario.class, funcionarioId);
	}
}
