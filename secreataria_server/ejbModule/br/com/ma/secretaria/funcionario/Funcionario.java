package br.com.ma.secretaria.funcionario;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;

import br.com.secretaria.municipio.Municipio;
import br.com.secretaria.tiposangeEnum.TipoSangeEnum;

@Entity(name = Funcionario.NAME)
@Table(name = "educacao.educ_funcionario")

@NamedQueries({@NamedQuery(name="funcionario.lista",query="SELECT f FROM "+Funcionario.NAME+" f")})
public class Funcionario implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String NAME = "educ_funcionario";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "funcionario_id")
	private Long funcionarioId;

	@NotEmpty(message = " O campo ' nome'  � obrigatorio !!")
	@Column(name = "func_nome", nullable = false)
	private String nome;

	@NotEmpty(message = " o campo 'CPF' obrigatorio !!")
	@Column(name = "func_cpf", unique = true, nullable = false)
	@CPF(message = "CPF invalido !!")
	private String cpf;

	@Column(name = "func_email", nullable = false, length = 100)
	@Email(message = "Email Invalido !!")
	private String email;

	@Column(name = "func_rg", nullable = false)
	private String rg;

	@Column(name = "func_tipo_sangue", nullable = false)
	@Enumerated(EnumType.STRING)
	private TipoSangeEnum tipoSangeEnum;

	@Column(name = "func_data_nascimento")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataNascimento;

	@Column(name = "func_barrio")
	@NotEmpty(message = "o campo 'barrio' � obrigatorio !!")
	private String barrio;

	
	@NotNull(message="Selecione um municipio")
	@ManyToOne
	@JoinColumn(name="func_municipio")
	private Municipio municipio;

	@Column(name = "func_endereco")
	private String endereco;

	@Column(name = "func_numero_casa")
	private String numeroCasa;

	@Column(name = "func_telefone1")
	private String telefone;

	@Column(name = "func_telefone2")
	private String telefone2;
	
	@Column(name = "func_genero")
    private String genero;
	
	
	@Transient
	public String getPrimeiroNome() {
		Integer index  =  this.nome.indexOf(" ");
		String primeiroNome  =  this.nome.substring(0,index);
		return primeiroNome;
	}

	
	public String getGenero() {
		return genero;
	}




	public void setGenero(String genero) {
		this.genero = genero;
	}




	public Long getFuncionarioId() {
		return funcionarioId;
	}

	public void setFuncionarioId(Long funcionarioId) {
		this.funcionarioId = funcionarioId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public TipoSangeEnum getTipoSangeEnum() {
		return tipoSangeEnum;
	}

	public void setTipoSangeEnum(TipoSangeEnum tipoSangeEnum) {
		this.tipoSangeEnum = tipoSangeEnum;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

    
	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumeroCasa() {
		return numeroCasa;
	}

	public void setNumeroCasa(String numeroCasa) {
		this.numeroCasa = numeroCasa;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((funcionarioId == null) ? 0 : funcionarioId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (funcionarioId == null) {
			if (other.funcionarioId != null)
				return false;
		} else if (!funcionarioId.equals(other.funcionarioId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Funcionario [funcionarioId=" + funcionarioId + ", nome=" + nome + ", cpf=" + cpf + ", tipoSangeEnum="
				+ tipoSangeEnum + ", dataNascimento=" + dataNascimento + ", municipio=" + municipio + "]";
	}
	
	

}
