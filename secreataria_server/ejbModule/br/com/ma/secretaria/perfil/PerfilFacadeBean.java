package br.com.ma.secretaria.perfil;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.secretaria.usuario.Usuario;

@Stateless
public class PerfilFacadeBean {
@PersistenceContext(unitName ="educacaoPU")
private EntityManager manager;

public Perfil findPrimaryKey(Integer perfilId) {
	return manager.find(Perfil.class, perfilId);
}


public List<Perfil> findAll(){
	TypedQuery<Perfil> query = manager.createQuery("SELECT p FROM "+Usuario.NAME+" p ",Perfil.class); 
	
	return query.getResultList();
}

}
