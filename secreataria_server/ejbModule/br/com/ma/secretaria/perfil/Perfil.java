package br.com.ma.secretaria.perfil;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.secretaria.unidade.Unidade;

@Entity(name=Perfil.NAME)
@Table(name="educacao.educ_perfil")
public class Perfil implements Serializable {


	private static final long serialVersionUID = 1166457394733329863L;
	public static final String NAME = "educ_perfil";
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="perfil_id")
	private Integer perfilId;
	@NotNull
	@Column(name = "descricao")
	private String descricao;
	public Integer getPerfilId() {
		return perfilId;
	}
	public void setPerfilId(Integer perfilId) {
		this.perfilId = perfilId;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	 
	
	
	
	
	
	
	
}
