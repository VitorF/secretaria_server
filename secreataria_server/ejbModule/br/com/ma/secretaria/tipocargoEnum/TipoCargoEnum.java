package br.com.ma.secretaria.tipocargoEnum;

public enum TipoCargoEnum {

	EFETIVO("Efetivo") ,COMISSIONADO("Comissionado");
	
	private String valor;
	TipoCargoEnum(String valor){
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}
}
